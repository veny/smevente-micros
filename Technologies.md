# Podman
* [Oracle Linux - Podman User's Guide](https://docs.oracle.com/en/operating-systems/oracle-linux/podman/index.html)

## Storage
* https://docs.oracle.com/en/operating-systems/oracle-linux/podman/configuring-podman-storage.html

## Networking
* https://podman.io/getting-started/network
* https://github.com/containers/podman/blob/main/docs/tutorials/basic_networking.md

## Tips & Trics
```bash
# volume management
podman volume ls
podman volume rm foo

# pod management
podman pod ls
podman pod rm smevente
podman pod stats
```

# Keycloak
* why? [Top Open Source Projects for User Management](https://frontegg.com/blog/top-user-management-open-source-projects)
* [Get started with Keycloak on Podman](https://www.keycloak.org/getting-started/getting-started-podman)


# PostgreSQL
* [Docker image](https://hub.docker.com/_/postgres)

# Redis
* [Docker image](https://hub.docker.com/_/redis)

# KrakenD
* [Installing with Docker](https://www.krakend.io/docs/overview/installing/#docker)


# 3rd party dependencies
| Name | Type | Where | Version |
| ---- | ---- | ----- | ------- |
| PostgreSQL | Docker image | podman | postgres:14.0-alpine (21-11-01) |
| Keycloak | Docker image | podman | quay.io/keycloak/keycloak:15.0.2 (21-11-01) |
| redis | Docker image | podman | docker.io/redis:6.2.6-alpine (21-11-19) |
| Python | Docker image | ping_service/Dockerfile | python:3.10.0-alpine (21-11-02) |
| !flask | Python lib | ping_service/requirements.txt | ~= 2.0.0 (21-11-14) |
| !Flask-OIDc | Python lib | api_gateway/requirements.txt | >= 1.4.0 (21-11-09) |
| !requests | Python lib | api_gateway/requirements.txt | >= 2.26.0 (21-11-09) |
| redis | Python lib | prj_pytools/requirements.txt | >= 4.0.1 (21-11-19) |
