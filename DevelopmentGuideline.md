# Dev Environment

## Podman
* activate [Rootless Podman](https://wiki.archlinux.org/title/Podman#Rootless_Podman)
* update /etc/containers/registries.conf
```
unqualified-search-registries = ["docker.io"]
```

### Run app
```bash
podman volume create postgres_data
podman volume create redis_data

podman secret create idp_keycloak conf/idp_keycloak.secret

podman pod create --name smevente -p 8080:8080 -p 5010:5010 -p 6379:6379

# postgres
podman run -d --pod=smevente --name postgres \
    -e POSTGRES_PASSWORD=password \
    -v postgres_data:/var/lib/postgresql/data \
    -v ./postgres/init-db.sh:/docker-entrypoint-initdb.d/init-db.sh:ro \
    postgres:14.0-alpine

# keycloak
podman run -d --pod=smevente --name keycloak \
    --requires=postgres \
    -e KEYCLOAK_USER=admin \
    -e KEYCLOAK_PASSWORD=admin \
    -e DB_VENDOR=POSTGRES \
    -e DB_ADDR=localhost \
    -e DB_DATABASE=keycloak \
    -e DB_USER=keycloak \
    -e DB_SCHEMA=public \
    -e DB_PASSWORD=password \
    quay.io/keycloak/keycloak:15.0.2
# -Djboss.http.port=8090

# redis
podman run -d --pod=smevente --name redis \
    -v redis_data:/data \
    docker.io/library/redis:6.2.6-alpine

# ping_service
podman run -d --pod=smevente --secret idp_keycloak \
    --requires=redis \
    -e IDP_SECRET=/run/secrets/idp_keycloak \
    --name ping_service localhost/smevente_ping:0.1.0
```


## Python

```bash
python -m venv .env
# api_gateway dependencies
pip install -r api_gateway/requirements.txt
```
