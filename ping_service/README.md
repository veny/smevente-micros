# Ping Service

This is a simplest example of a microservice.
Purpose of this service is to be used in other spikes.

## Build

```bash
podman build -t smevente_ping:0.1.0 -f ping_service/Containerfile .
```

## Run

```bash
podman run -d --pod=smevente --secret idp_keycloak \
    -e IDP_SECRET=/run/secrets/idp_keycloak \
    --name ping_service localhost/smevente_ping:0.1.0
```

## Versions
| Version | Date | Description |
| ------- | ---- | ----------- |
| 0.1.0 | 21-11-02 | Initial revision |
