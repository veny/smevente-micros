import os
import logging.config

from flask import Flask, request, g, url_for, jsonify

from prj_pytools.conf import DEFAULT_LOGGING_CONFIG, load_idp_secret_config
from prj_pytools.jwt_authz import JwtAutz
from prj_pytools.service_discovery import ServiceDiscovery


SERVICE_NAME = "ping"

# SETUP
logging.config.dictConfig(DEFAULT_LOGGING_CONFIG)
idp_secret_config = load_idp_secret_config()


app = Flask(SERVICE_NAME)
jwt_authz = JwtAutz(
    idp_secret_config["DEFAULT"]["ServerUrl"],
    idp_secret_config["DEFAULT"]["RealmName"])


@app.route("/ping")
@jwt_authz.token_required
def ping():
    """Replies always with 'pong'."""
    if g.jwt_data is None:
        raise Exception("JWT data not found in context")
    app.logger.debug(g.jwt_data)
    return "pong\n"


@app.route("/health")
def health():
    '''Health check.'''
    if True: # there should be a regular condition of the service
        resp = jsonify(health="healthy")
        resp.status_code = 200
    else:
        resp = jsonify(health="unhealthy")
        resp.status_code = 500

    return resp
    return jsonify(rslt)


if __name__ == "__main__":
    port = int(os.environ.get("PORT", 5010))
    app.logger.info(f"{SERVICE_NAME.upper()} starting...")
    app.sd = ServiceDiscovery()
    app.sd.register_service(SERVICE_NAME, f"http://localhost:{port}", "REST", "JWT", "Demo service")
    app.run(host="0.0.0.0", port=port)
