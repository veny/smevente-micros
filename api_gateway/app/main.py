#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import string
import pathlib
import logging.config
import configparser

from flask import Flask, request, Response, redirect, url_for, g, jsonify
from flask_oidc import OpenIDConnect
import urllib.parse

from prj_pytools.conf import DEFAULT_LOGGING_CONFIG, load_idp_secret_config
from prj_pytools.jwt_authz import JwtAutz
from prj_pytools.service_discovery import ServiceDiscovery
from reverse_proxy import ReverseProxy


SERVICE_NAME = "api_gateway"

# SETUP
logging.config.dictConfig(DEFAULT_LOGGING_CONFIG)
idp_secret_config = load_idp_secret_config()

# generate the 'client_secrets.json'
client_secret_template = '''{
    "web": {
        "issuer": "$ServerUrl/auth/realms/$RealmName",
        "auth_uri": "$ServerUrl/auth/realms/$RealmName/protocol/openid-connect/auth",
        "client_id": "$ClientId",
        "client_secret": "$ClientSecret",
        "redirect_uris": [
            "$RedirectUris"
        ],
        "userinfo_uri": "$ServerUrl/auth/realms/$RealmName/protocol/openid-connect/userinfo", 
        "token_uri": "$ServerUrl/auth/realms/$RealmName/protocol/openid-connect/token",
        "token_introspection_uri": "$ServerUrl/auth/realms/$RealmName/protocol/openid-connect/token/introspect"
    }
}'''
client_secrets_file = "client_secrets.json"
# generate the file content
file_content = string.Template(client_secret_template).substitute(
    ServerUrl=idp_secret_config["DEFAULT"]["ServerUrl"],
    RealmName=idp_secret_config["DEFAULT"]["RealmName"],
    ClientId=idp_secret_config["DEFAULT"]["ClientId"],
    ClientSecret=idp_secret_config["DEFAULT"]["ClientSecret"],
    RedirectUris=idp_secret_config["DEFAULT"]["RedirectUris"])
# write the file
with open(f"{pathlib.Path(__file__).parent.resolve()}/{client_secrets_file}", 'w') as f:
    f.write(file_content)


app = Flask(SERVICE_NAME)
app.config.update({
    'SECRET_KEY': idp_secret_config["DEFAULT"]["SecretKey"],
    'TESTING': True,
    'DEBUG': True,
    'OIDC_CLIENT_SECRETS': client_secrets_file,
    'OIDC_ID_TOKEN_COOKIE_SECURE': False,
    'OIDC_REQUIRE_VERIFIED_EMAIL': False,
    'OIDC_USER_INFO_ENABLED': True,
#    'OIDC_OPENID_REALM': 'smevente',
    'OIDC_SCOPES': ['openid', 'email', 'profile'],
    'OIDC_INTROSPECTION_AUTH_METHOD': 'client_secret_post'
})

oidc = OpenIDConnect(app)
rp = ReverseProxy()


@app.before_request
def before_request():
    if oidc.user_loggedin and oidc.get_access_token() is not None: # token is None when expired
        g.user = oidc.user_getfield("email")
        # Keycloak access token is a JWT
        # see in https://jwt.io/
        g.access_token = oidc.get_access_token()
    else:
        app.logger.warning("User not logged in or token expired")
        g.user = None
        g.access_token = None


@app.route("/")
def index():
    if oidc.user_loggedin:
        return f"Welcome {g.user} on {url_for('index', _external=True)}"
    else:
        return f"Not logged in on {url_for('index', _external=True)}"


@app.route("/login")
@oidc.require_login
def login():
    """Performs login."""
    app.logger.info(f"logging in, user={g.user}")
    return f"Welcome {oidc.user_getfield('email')}"


@app.route("/logout")
def logout():
    """Performs logout."""
    app.logger.info("logging out...")
    oidc.logout()
    redirect_url = url_for("index", _external=True)
    # https://suedbroecker.net/2021/05/18/simply-logout-from-keycloak/
    app.logger.debug(f"redirecting to: {redirect_url}")
    return redirect(f"{idp_secret_config['DEFAULT']['ServerUrl']}/auth/realms/{idp_secret_config['DEFAULT']['RealmName']}/protocol/openid-connect/logout?redirect_uri={urllib.parse.quote(redirect_url)}")


@app.route("/health")
def health():
    '''Health check.'''
    rslt = {}
    # redis
    rslt['redis'] = "healthy" if app.sd.ping() else "unhealthy"
    # keycloak
    rslt['keycloak'] = "healthy" if app.sd.url_health_check(f"{idp_secret_config['DEFAULT']['ServerUrl']}/auth/realms/{idp_secret_config['DEFAULT']['RealmName']}") else "unhealthy"

    # services
    services = app.sd.list_services()
    for s in services:
        healthy = app.sd.service_health_check(s)
        rslt[s] = "healthy" if healthy else "unhealthy"

    return jsonify(rslt)


@app.route("/temp", methods=['GET'])
def temp():
    print(request.headers)
    print("======================")
    print(request.cookies)
    print("======================")
    print(oidc.get_access_token())
    return "OK"


@app.route("/proxy", methods=['GET'])
@oidc.require_login
@rp.pipe(service="ping", path="/ping")
def proxy():
    app.logger.info("ping proxy")


if __name__ == '__main__':
    port = int(os.environ.get("PORT", 5000))
    app.logger.info(f"{SERVICE_NAME.upper()} starting...")
    app.sd = ServiceDiscovery()
    app.run(host="0.0.0.0", port=port)
