class IdentityProviderInterface:
    """Intervace to define expectations of an IdentityProvider."""

    def get_token_by_password(self, username: str, password: str) -> str:
        """Get an access token from IdP."""
        pass

    def validate(self, token: str) -> bool:
        """Validates given access token."""
        pass


def get_idp(name: str, config: dict) -> IdentityProviderInterface:
    """Factory method to get an instance of IdP."""
    if name.upper() == "KEYCLOAK":
        from keycloak import KeycloakIdentityProvider
        return KeycloakIdentityProvider(
            server_url=config["server_url"],
            realm_name=config["realm_name"],
            client_id=config["client_id"],
            client_secret=config["client_secret"])
    else:
        raise ValueError(name)
