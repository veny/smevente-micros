#!/usr/bin/env python3

from api_idp import IdentityProviderInterface, get_idp

iprovider = get_idp("Keycloak",
                        {
                            "server_url" : "localhost:8090",
                            "realm_name" : "smevente",
                            "client_id": "krakend-client",
                            "client_secret": "94569cfe-9f87-4c71-932c-13aa56d9353b", 
                        })


# Realm Settings / Keys / Algorithm-RS256,Use=SIG / Public key
# or extract from http://localhost:8090/auth/realms/smevente/protocol/openid-connect/certs
public_key = """-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAv9ePfVx2sLu1euFQEU8tJkMyPayRfFHpDK1Od09v4mh9/y/pVZSrSzTFG2Re9aIPOw3y9cIB9OJBUSaR/0tb/3GaPb0OTwxkKE3HkS3bE9bqzwPASuQ/cGostFsY17a/jvXvxdANc4Lx7SvzAasC9qG7s76TFDHjgOvIpX+6pSSLDGd/uXMMlXEtiJ8DXWbD+4RL2LBbbiHqOI/CV4usSz2cK6usWwssoYkv0fChJh9U68IKnprCCE5urlQoRiLiUb5fE8+jh+RgwD61psK21f3gBb23ezEXJYCUMDax0ESaa095SWEQUs4Fps8HAEIl8Cq0DdQmPZ20vRbJT+C/2QIDAQAB
-----END PUBLIC KEY-----"""

#response = requests.post(f"http://{server_url}/auth/realms/{realm_name}/protocol/openid-connect/token", data=data)
#if response:
access_token = iprovider.get_token_by_password("veny", "UNKNOW")
print(access_token + "\n---")

iprovider.validate(access_token)
