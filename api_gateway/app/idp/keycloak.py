from api_idp import IdentityProviderInterface
import requests
import jwt
from jwt import PyJWKClient

class KeycloakIdentityProvider(IdentityProviderInterface):

    def __init__(self, server_url: str, realm_name: str, client_id: str, client_secret: str):
        self.server_url = server_url
        self.realm_name = realm_name
        self.client_id = client_id
        self.client_secret = client_secret
    
    def get_token_by_password(self, username: str, password: str) -> str:
        # https://stackoverflow.com/questions/53538100/how-to-get-client-secret-via-keycloak-api
        data = {
            "username": username,
            "password": password,
            "client_id": self.client_id,
            "client_secret": self.client_secret, 
            "grant_type": "password"
        }

        response = requests.post(f"http://{self.server_url}/auth/realms/{self.realm_name}/protocol/openid-connect/token", data=data)
        if response:
            return response.json()["access_token"]
        else:
            print(f"failed to get access_token, rcode={response.status_code}, content={response.content}")
            return 'XXX'
            

    def validate(self, access_token: str) -> bool:
        jwks_client = PyJWKClient(f"http://{self.server_url}/auth/realms/{self.realm_name}/protocol/openid-connect/certs")
        # algorithm is stored in the token, e.g. {"alg":"RS256","typ" : "JWT","kid" ...
        signing_key = jwks_client.get_signing_key_from_jwt(access_token)
        #print(signing_key.key.__dict__)

        access_token_json = jwt.decode(access_token, signing_key.key, algorithms=['RS256'], audience='account')
        print(access_token_json)
        return True

