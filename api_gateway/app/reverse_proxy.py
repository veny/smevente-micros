import logging
from functools import wraps

import flask
import requests
from werkzeug.exceptions import Unauthorized


class ReverseProxy:
    """This class represents a set of tools for reverse proxy."""

    def pipe(function, service, path):
        def real_decorator(function):
            @wraps(function)
            def wrapper(*args, **kwargs):
                logging.debug(f"reverse proxy forwarding, service={service}, path={path}")
                if flask.g.access_token is None:
                    logging.error("Access token not found in context")
                    raise Unauthorized("Access token not found")
                else:
                    logging.info(f"Access token routed: {flask.g.access_token}")

                # Service Discovery
                service_about = flask.current_app.sd.lookup_service(service)

                # call the original function, but the 'retval' is ignored
                retval = function(*args, **kwargs)

                if flask.request.method=='GET':
                    headers = {"Authorization": f"Bearer {flask.g.access_token}"}
                    resp = requests.get(f"{service_about['endpoint']}{path}", headers=headers)
                    excluded_headers = ['content-encoding', 'content-length', 'transfer-encoding', 'connection']
                    headers = [(name, value) for (name, value) in resp.raw.headers.items() if name.lower() not in excluded_headers]
                    response = flask.Response(resp.content, resp.status_code, headers)
                    logging.debug(f"status code: {resp.status_code}")
                    return response
                return retval
            return wrapper
        return real_decorator
