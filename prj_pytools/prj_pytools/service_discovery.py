import logging
from datetime import datetime, timedelta

import redis
import requests


class ServiceDiscovery:
    """
    Service Registration/Discovery with Redis

    Key space organization:
    Prefix : Component Name : Type

    * The Prefix segment allows for filtering Apps vs non-Apps keys.
    * The Component Name segment aids in filtering keys of a particular component.
    * [Not Implemented Yet] The Instance ID segment allows filtering keys for a unique service instance.
      When running microservices you typically want multiple instances of a service type running.
      Each service instance is assigned a unique ID and being able to differentiate between them is useful.
    * The Type segment which is used to classify the purpose of a key.
    """

    def __init__(self, host='localhost', port=6379, db=0, auth=None, prefix='smevente', chache_ttl_min=5):
        self.prefix = prefix
        self.redis = redis.Redis(host=host, port=port, db=db, decode_responses=True)
        self.cache = {}
        self.chache_ttl_min = chache_ttl_min


    def register_service(self, service_name, endpoint, endpoint_type='', auth_type=None, description=''):
        '''Registers a service.'''
        if service_name is None or endpoint is None:
            raise TypeError("invalid arguments")

        about = {
            "endpoint" : endpoint,
            "endpoint_type" : endpoint_type,
            "auth_type" : auth_type,
            "description" : description
        }
        key = f"{self.prefix}:{service_name}:service"
        self.redis.hset(key, mapping=about)
        logging.info(f"service registered, key={key} | {about}")
        return about


    def lookup_service(self, service_name):
        '''Lookup a service by name.'''
        key = f"{self.prefix}:{service_name}:service"
        if key in self.cache and self.cache[key][0] > datetime.now()-timedelta(minutes=self.chache_ttl_min):
            logging.debug(f"service '{service_name}' found in cache: {key}")
            return self.cache[key][1]

        if not self.redis.exists(key):
            raise KeyError(key)
        
        about = self.redis.hgetall(key)
        logging.info(f"service discovered, key={key} | {about}")
        self.cache[key] = (datetime.now(), about)
        return about


    def list_services(self):
        '''Returns list of all registered services.'''
        services = []
        for s in self.redis.scan_iter(f"{self.prefix}:*:service"):
            x = s.split(':')
            if x[1] not in services:
                services.append(x[1])
        logging.info(f"service list: {services}")
        return services


    def ping(self):
        '''Pings the Redis server.'''
        return self.redis.ping()


    def service_health_check(self, service_name):
        '''Performs a health check on given service.'''
        about = self.lookup_service(service_name)
        return self.url_health_check(f"{about['endpoint']}/health")


    def url_health_check(self, url):
        '''Performs a health check on given URL.'''
        resp = requests.get(url)
        logging.info(f"health check: url={url}, status_code={resp.status_code}")
        return True if resp.status_code == 200 else False


    # TODO
    #def remove_service(self, service_name):


if __name__ == "__main__":
    sd = ServiceDiscovery()
    sd.register_service("ping", "http://localhost:5010", "REST", "JWT", 'Demo service')
    print(sd.ping())
    print(sd.lookup_service("ping"))
    print(sd.list_services())
