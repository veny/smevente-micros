import logging
from datetime import datetime, timedelta

from flask import request, g
from werkzeug.exceptions import Unauthorized
import jwt
from jwt import PyJWKClient


class JwtAutz():
    """This class represents a set of tools for JWT processing."""

    def __init__(self, server_url, realm_name):
        self.server_url = server_url
        self.realm_name = realm_name
        self.jwks_client = PyJWKClient(f"{self.server_url}/auth/realms/{self.realm_name}/protocol/openid-connect/certs")
        self.cached_signing_key = None


    def token_required(self, func):
        """Decorator for flask routes which need JWT token."""
        def inner():
            self._verify_authN()
            rslt = func()
            return rslt
        return inner


    def _verify_authN(self):
        if "Authorization" not in request.headers or not request.headers["Authorization"].startswith("Bearer "):
            raise Unauthorized("Access denied: missing token")
        access_token = request.headers["Authorization"].replace("Bearer ", '')
        logging.info(f"Bearer found: {access_token[:10]}...")
        try:
            signing_key = self._get_signing_key(access_token)
            jwt_data = jwt.decode(access_token, signing_key.key, algorithms=['RS256'], audience='account')
        except Exception as e:
            logging.exception("Failed to validate token")
            raise Unauthorized(f"Failed to validate token: {str(e)}")
        logging.debug("JWT successfuly validated")
        g.jwt_data = jwt_data
        return jwt_data

    def _get_signing_key(self, access_token):
        if not self.cached_signing_key or self.cached_signing_key[0] < datetime.now()-timedelta(hours=24):
            logging.info(f"Signing key not found in cache, loading from {self.server_url}")
            # algorithm is stored in the token, e.g. {"alg":"RS256","typ" : "JWT","kid" ...
            signing_key = self.jwks_client.get_signing_key_from_jwt(access_token)
            # store with datetime
            self.cached_signing_key = (datetime.now(), signing_key)
            return signing_key
        else:
            logging.debug("Signing key found in cache")
            return self.cached_signing_key[1]
