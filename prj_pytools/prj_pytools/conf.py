import os
import logging
import configparser


DEFAULT_LOGGING_CONFIG = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {'default': {
        'format': '%(asctime)s [%(levelname)s] %(name)s (%(filename)s) %(threadName)s | %(message)s',
    }},
    'handlers': {'default': {
        'class': 'logging.StreamHandler',
        'stream': 'ext://sys.stdout',
        'formatter': 'default'
    }},
    'root': {
        'level': 'DEBUG',
        'handlers': ['default']
    }
}


def load_idp_secret_config():
    '''
    Loads configuration file with the IdP information.
    '''
    idp_secret_config = configparser.ConfigParser()
    idp_secret = os.environ["IDP_SECRET"]
    logging.info(f"IdP secret file={idp_secret}")
    with open(idp_secret) as f:
        idp_secret_config.read_file(f)
    return idp_secret_config
